# github.com/andybalholm/brotli v1.0.2
## explicit; go 1.12
github.com/andybalholm/brotli
# github.com/dgrijalva/jwt-go v3.2.0+incompatible
## explicit
github.com/dgrijalva/jwt-go
# github.com/go-sql-driver/mysql v1.6.0
## explicit; go 1.10
github.com/go-sql-driver/mysql
# github.com/gofiber/fiber v1.14.6
## explicit; go 1.11
# github.com/gofiber/fiber/v2 v2.18.0
## explicit; go 1.16
github.com/gofiber/fiber/v2
github.com/gofiber/fiber/v2/internal/bytebufferpool
github.com/gofiber/fiber/v2/internal/colorable
github.com/gofiber/fiber/v2/internal/encoding/ascii
github.com/gofiber/fiber/v2/internal/encoding/json
github.com/gofiber/fiber/v2/internal/isatty
github.com/gofiber/fiber/v2/internal/schema
github.com/gofiber/fiber/v2/internal/uuid
github.com/gofiber/fiber/v2/middleware/cors
github.com/gofiber/fiber/v2/utils
# github.com/gofiber/utils v0.0.10
## explicit; go 1.11
# github.com/gorilla/schema v1.1.0
## explicit
# github.com/jinzhu/inflection v1.0.0
## explicit
github.com/jinzhu/inflection
# github.com/jinzhu/now v1.1.2
## explicit; go 1.12
github.com/jinzhu/now
# github.com/klauspost/compress v1.13.4
## explicit; go 1.13
github.com/klauspost/compress/flate
github.com/klauspost/compress/gzip
github.com/klauspost/compress/zlib
# github.com/mattn/go-colorable v0.1.7
## explicit; go 1.13
# github.com/mattn/go-isatty v0.0.12
## explicit; go 1.12
# github.com/valyala/bytebufferpool v1.0.0
## explicit
github.com/valyala/bytebufferpool
# github.com/valyala/fasthttp v1.29.0
## explicit; go 1.12
github.com/valyala/fasthttp
github.com/valyala/fasthttp/fasthttputil
github.com/valyala/fasthttp/reuseport
github.com/valyala/fasthttp/stackless
# github.com/valyala/tcplisten v1.0.0
## explicit; go 1.12
github.com/valyala/tcplisten
# golang.org/x/crypto v0.0.0-20210915214749-c084706c2272
## explicit; go 1.17
golang.org/x/crypto/bcrypt
golang.org/x/crypto/blowfish
# golang.org/x/sys v0.0.0-20210615035016-665e8c7367d1
## explicit; go 1.17
golang.org/x/sys/internal/unsafeheader
golang.org/x/sys/unix
golang.org/x/sys/windows
# gorm.io/driver/mysql v1.1.2
## explicit; go 1.14
gorm.io/driver/mysql
# gorm.io/gorm v1.21.15
## explicit; go 1.14
gorm.io/gorm
gorm.io/gorm/callbacks
gorm.io/gorm/clause
gorm.io/gorm/logger
gorm.io/gorm/migrator
gorm.io/gorm/schema
gorm.io/gorm/utils
